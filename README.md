# nortek_dvl_snitch

A ROS node which performs two functions:

* At startup, it connects to a Nortek DVL's command interface over Ethernet (default to port 9000), and sends a set of commands which set transmit power to "off"

* It then listens to the binary data connection (default to port 9001), receives an unpacks the DF21 data packets, and prints a warning **if the velocity of depth data is VALID.**   The data is expected to be invalid if the transmitter is off.

## Installation

This package requires the non-apt-gettable python package `asyncio-telnet`:

```
pip3 install asyncio-telnet
```

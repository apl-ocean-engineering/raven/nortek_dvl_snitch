#!/usr/bin/env python3

import asyncio
from asyncio_telnet import Telnet
import re
import rospy
import struct


class NortekConfigurator:
    def __init__(self, host, port=9000):
        self.host = host
        self.port = port

    async def run(self):

        rospy.loginfo("Connecting to DVL ...")

        tn = Telnet()
        await tn.open(self.host, port=self.port, read_timeout=100)
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        if not re.search(r"Username:", response):
            rospy.logwarn('Did not get "username" prompt from DVL, giving up')
            return

        await tn.write("nortek\r\n".encode())
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        if not re.search(r"Password:", response):
            rospy.logwarn('Did not get "username" prompt from DVL, giving up')
            return

        await tn.write("\r\n".encode())
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        ## Send a CTRL-C
        await tn.write("\x03".encode())

        # Can take a while for DVL to wake up
        count = 0
        max_count = 25
        while True:
            response = await tn.read_until_eof(decode=True)
            rospy.logdebug(f"Got response {response!r}")

            if re.search("OK", response):
                break

            count = count + 1
            if count > max_count:
                rospy.loginfo("Could not wake DVL, returning...")
                return

        await tn.write("MC\r\n".encode())
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        # Test command which sets non-zero transmit power
        # dvl_commands = ["SETBT,PL=-20\r\n"]
        rospy.loginfo("Configuring DVL ...")

        dvl_commands = [
            "SETBT,PL=-100\r\n",
            'SETBT,PLMODE="USER"\r\n',
            "SETDVL,SR=8\r\n",
        ]

        for dvl_cmd in dvl_commands:
            await tn.write(dvl_cmd.encode())
            response = await tn.read_until_eof(decode=True)
            rospy.logdebug(f"Got response {response!r}")

            response = response.strip()

            if response != "OK":
                rospy.logwarn(
                    f'Did not get "OK" in response to command "{dvl_cmd.strip()}'
                )

        await tn.write("GETBT\r\n".encode())
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        # Typical response to GETBT is (not this includes
        # the "OK" in response to the request):
        #
        #  '50.00,5.00,4,0,21,-100.0,"OFF",22,0.01,"USER"\r\nOK\r\n'
        #
        # Fields are:
        #   - Bottom track range (50m in example above)
        #   - Velocity range (5 m/s)
        #   - Number of beams (4)
        #   - Select beams (0 == use all beams)
        #   - Data format (DF21)
        #   - Transmit power (-20 - 0db,  -100 to switch off transmitter)
        #   - Measure water track velocity ("OFF")
        #   - Water track data format (DF22)
        #   - Blanking distance (0.01 m)
        #   - Power level mode ("USER" / "MAX")
        #
        getbt_resp = re.sub(r"\.[\r\n]$", "", response)

        rospy.loginfo(f"GETBT: {getbt_resp}")
        bt_params = getbt_resp.split(",")

        if len(bt_params) == 10:
            if float(bt_params[5]) != -100.0:
                rospy.logwarn(
                    f"Transmit power not set to -100, DVL reports {bt_params[5]}"
                )
        else:
            rospy.logwarn(
                f"Unexpected number of fields in getbt response {len(bt_params)} != 10"
            )

        await tn.write("START\r\n".encode())
        response = await tn.read_until_eof(decode=True)
        rospy.logdebug(f"Got response {response!r}")

        await tn.close()
        return response


class NortekBinaryClient:

    def __init__(self, host: str, port=9001) -> None:
        self.host = host
        self.port = port

    async def run(self) -> None:
        reader, writer = await asyncio.open_connection(self.host, self.port)

        data = await reader.read(100)
        rospy.logdebug(f"Received: {data.decode()!r}")

        writer.write("nortek\r\n".encode())

        data = await reader.read(100)
        rospy.logdebug(f"Received: {data.decode()!r}")

        writer.write("\r\n".encode())

        data = await reader.read(100)
        rospy.logdebug(f"Received: {data.decode()!r}")

        if re.search(r"Nortek.*Command Interface", data.decode()):
            rospy.loginfo("Good connection to DVL data socket")
            await self.run_data_loop(reader, writer)

        rospy.loginfo("Close the connection")
        writer.close()
        await writer.wait_closed()

    async def run_data_loop(self, reader, writer):
        while True:
            data = await reader.read(1024)

            header_struct = "<4B3H"
            header_size = struct.calcsize(header_struct)

            if len(data) < header_size:
                continue

            # For now, assume we only get whole packets
            delim, hdr_sz, pkt_type, _, data_sz, _chksum_data, _chksum_header = (
                struct.unpack(header_struct, data[:header_size])
            )

            if delim != 165:
                rospy.loginfo(f"Invalid delimiter {delim}, ignoring")
                continue

            if hdr_sz != header_size:
                rospy.loginfo(f"Invalid header size {hdr_sz}, ignoring")
                continue

            # rospy.loginfo(f"Packet type {pkt_type:02X}")

            if pkt_type == 0x1B:
                self.parse_dvl_bottom_track(data[header_size:])

    def parse_dvl_bottom_track(self, buffer):

        df21_struct = "<2BI6B2H2I47f"
        df21_size = struct.calcsize(df21_struct)

        if len(buffer) != df21_size:
            rospy.loginfo(
                f"Packet incorrect length for df21 packet ({len(buffer)} != {df21_size})"
            )
            return

        (
            version,
            _,
            serial_number_,
            year_,
            month_,
            day_,
            hour_,
            min_,
            sec_,
            microsec_,
            num_beams_,
            df21_error,
            df21_status,
            sound_speed_,
            temperature_,
            pressure,
            vel_beam1,
            vel_beam2,
            vel_beam3,
            vel_beam4,
            dist_beam1,
            dist_beam2,
            dist_beam3,
            dist_beam4,
            *everything_else_,
        ) = struct.unpack(df21_struct, buffer)

        vel_beam = (vel_beam1, vel_beam2, vel_beam3, vel_beam4)
        dist_beam = (dist_beam1, dist_beam2, dist_beam3, dist_beam4)

        if version != 1:
            rospy.loginfo(f"Unexpected version {version} != 1")
            return

        rospy.loginfo_once(
            "Receiving good df21 data from DVL, monitoring for non-invalid data"
        )

        rospy.logdebug(f"Pressure {pressure:.1f} bar")
        rospy.logdebug(
            f"Beam velocities: {vel_beam1:.2f}, {vel_beam2:.2f}, {vel_beam3:.2f}, {vel_beam4:.2f} m/s"
        )
        rospy.logdebug(
            f"Beam distances: {dist_beam1:.2f}, {dist_beam2:.2f}, {dist_beam3:.2f}, {dist_beam4:.2f} m/s"
        )

        # From the Nortek Manual, these values indicate "invalid velocity"
        # and "invalid distance" resp.
        invalid_vel = -32.768001
        invalid_dist = 0.00
        epsilon = 1e-2

        for i, vel in zip((1, 2, 3, 4), vel_beam):
            if abs(vel - invalid_vel) > epsilon:
                rospy.logwarn(f"Beam {i} has non-invalid velocity {vel}")

        for i, dist in zip((1, 2, 3, 4), dist_beam):
            if abs(dist - invalid_dist) > epsilon:
                rospy.logwarn(f"Beam {i} has non-invalid distance {dist}")


def main() -> None:
    dvl_hostname = "dvl.raven.apl.uw.edu"
    config_port = 9000
    data_port = 9001

    nortek_config = NortekConfigurator(dvl_hostname, config_port)
    asyncio.run(nortek_config.run())

    nortek_client = NortekBinaryClient(dvl_hostname, data_port)
    asyncio.run(nortek_client.run())
